<?php
	defined('BASEPATH' OR exit('No direct script access allowed'));

	class UserController extends CI_Controller
	{
		public function __construct()
		{
			parent:: __construct();
			$this->load->model("UserModel");
			$this->load->helper(array('form','url'));
		}

		public function index()
		{
			$this->load->view('user/loginUser');
		}
		//ir al form de login
		public function login()
		{
			$this->load->view('user/loginUser');
		}
		//logiarse
		public function logiarse()
		{
			$password=$this->input->post("password");
			$email=$this->input->post("email");
			$usuario = $this->UserModel->login($email,$password);
			
			
			
			if(isset($usuario) && !empty($usuario))
			{
				$info['usuario']=$usuario;
				$this->load->view('user/index',$info);
			}else
			{
				$info['respuesta']='no se encontro el usuario';
				$this->load->view('user/loginUser',$info);
			}
		}


		//ir al formulario de registro
		public function formRegistro()
		{
			$this->load->view('user/registro');			
		}

		//registrar al usuario
		public function registrar()
		{
			$nombre=$this->input->post("name");
			$password=$this->input->post("password");
			$email=$this->input->post("email");

			$resultado = $this->UserModel->newUser($nombre,$password,$email);

			{
				if($resultado)
				{
					$this->login();
				}else
				{
					$info['respuesta']='no se pudo registrar el usuario';
					$this->load->view('user/loginUser',$info);
				}
			}
		}
		public function agregar()
		{
			$nombre=$this->input->post("name");
			$id=$this->input->post("id");
			$usuario = $this->UserModel->Agregar($nombre, $id);

			if($usuario)
			{
				$usuario = $this->UserModel->getById($id);
				$info['usuario']=$usuario;
				$this->load->view('user/index',$info);	
			}else
			{
				echo "error al subir la imagen";
			}
		}
		public function eliminar()
		{
			$id=$this->input->request("id");
			echo($id);
			die();
		}
	}


?>
