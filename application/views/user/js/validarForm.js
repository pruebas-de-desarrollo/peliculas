'use strict'

function validarForm()
{
	var email = document.querySelector('.email').value;
	var password = document.querySelector('.password').value;

	if(email.trim()=='' || email.trim().length==0)
	{
		alert('por favor llene el campo de correo');
		return false;
	}else if(password.trim()=='' || password.trim().length==0)
	{
		alert('por favor llene el campo de password');
		return false;
	}else
	{
		return true;
	}
}
