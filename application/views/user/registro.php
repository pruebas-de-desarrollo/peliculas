<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<title>Registro de usuarios</title>
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
	</head>
	<body>
		<div class="container-fluid">
			<div class="row  justify-content-center">
				<div class="col-4 align-self-center">
					<form method="post" method="post" action="<?php echo base_url('index.php/user/registro');?>" onsubmit="return validarForm()" class="rounded shadow-lg mt-4 p-3">
						<div class="mb-3">
							<label for="exampleInputPassword1"  class="form-label">Nombre</label>
							<input type="text" class="form-control" name="name" id="exampleInputEmail" require>
						</div>
						<div class="mb-3">
							<label for="exampleInputEmail1" class="form-label">correo electronio</label>
							<input type="email" name="email" class="form-control email" id="exampleInputEmail1" aria-describedby="emailHelp" require>
							<div id="emailHelp" class="form-text">Ingrese con su correo electronico.</div>
						</div>
						<div class="mb-3">
							<label for="exampleInputPassword1"  class="form-label">Password</label>
							<input type="password" class="form-control password" name="password" id="exampleInputPassword1" require>
						</div>
						<button type="submit" class="btn btn-primary">Registrarse</button>
					</form>
				</div>
			</div>
		</div>
		<script type="text/javaScript" src="<?php echo base_url()?>application/views/user/js/validarForm.js"></script>
	</body>
</html>
