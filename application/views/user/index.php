<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<!---css-->															
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>application/views/user/css/Styles.css"/>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <!--jquery-->
     <script type='text/javascript' src="<?php echo base_url()?>application/disenio/jquery-3.4.1.min.js"></script>

    <title>Hello, world!</title>
  </head>
  <body>

  <!---contenido de la pagina-->
	<div class="container position-relative">
		<div class="row justify-content-center p-2 bg-primary mt-2 mb-4 rounded ">
		<?php foreach($usuario as $user): 
			$peliculas =  explode(",",$user->peliculas);
			$id = $user->id;?>

			<div class="col-2">
				<h5 class="text-white">Bienvenido:  <?php echo $user->nombre?> </h5>
			</div>
			<div class="col-4">
				<h5 class="text-white">Bienvenido:  <?php echo $user->email?> </h5>
				<script>
				 var peliculas =<?php echo json_encode($peliculas);?>;
				 console.log(peliculas);
			 </script>
			</div>
			<?php endforeach;?>
		</div>
		<!---mostrar la informacion de las peliculas del usuario-->
		<div class="row justify-content-start  border border-3 border-primary position-relative ms-4 rouded">
			<div class="bg-primary position-absolute top-0 start-0 translate-middle rouded" style="width: 10rem;">
				<h6 class="text-white" >Mis Peliculas</h6>
			</div>
			<div class="col-12 my-2 py-2" id="contenido">
				
			</div>
			
		</div>
		<!--boton para buscar una pelicula-->
		<div class="row">
        	<div id='botonMas' class=" col-1 mb-2 text-center rounded-pill float-end position-fixed bottom-0 start-50 translate-middle-x shadow-lg">
        		<a href="#"><h1><b>+</b></h1></a>
        	</div>
		</div>
		<!--formulario que busca otra pelicula-->
		<form action=""  class="position-fixed border border-info  pt-2 m-2 bg-white position-absolute top-50 start-50 translate-middle shadow-lg" id='form' style="width: 30rem;">
			<div class="col-10 form-floating m-2">
				<input type='text' name='name' class="form-control inputName" id="floatingInput" placeholder="name@example.com">
				<label for="floatingInput" class="px-3 text-muted" ><b>Nombre de la pelicula</b></label>
				<span id="error_name"></span>
			</div>
			<div class="position-absolute top-0 end-0 px-2 m-1" id="botonForm">
				<h5>X</h5>
			</div>
			<div class="col-8 m-2 p-2 ">
				<button type="submit" class="btn btn-primary px-4">Buscar</button> 
			</div>
		</form>
	</div>

	<!--mostrar los resultados de la busquedad del usuario-->
	<form action="<?php echo base_url('index.php/user/agregar');?>" method='post' class="card position-absolute top-50 start-50 translate-middle rounded border border-4 border-warning shadow-lg" id='divBusqueda' style="width: 18.5rem;">
			<div id="busqueda">
				<h1>Nose encontro la Pelicula</h1>
			</div>
				<div class="position-fixed top-0 start-50 translate-middle-x bg-warning"  style="width: 100%;">
					<button type="submit" class="btn btn-outline-light">Agregar</button>
					<input type="hidden" name='id' value='<?php echo $user->id;?>'>
				<div class="position-absolute top-0 end-0 px-2 m-1 " id="botonCerrar">
					<h5>X</h5>
				</div>
			</div>
		</form>
    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>											
	<script type="text/javaScript" src="<?php echo base_url()?>application/disenio/js/Ajax.js"></script>
  </body>
</html>
	
