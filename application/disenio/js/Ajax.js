$(document).ready(()=>
{
	var url="http://www.omdbapi.com/";
	const key ='54874d4b';
	var contenido = $('#contenido');

	//peticion ajax
	function peticionAjaxGet(nombre,url,key,contenido)
	{
		$.getJSON(url,{t:nombre,apikey:key}, function(respuesta)
		{
			console.log(respuesta.Response);
			if(respuesta.Response ==='False')
			{
				$('#divBusquedad').css('display','none');
				$('#botonMas').css('display','block');
				alert("no se encontro ninguna pelicula");
			}else
			{
				agregarContenido(respuesta,contenido);
				
			}
			
		});
	}

	//funcion agregar contenido de la respuesta ajax a la vista
	function agregarContenido(respuesta,etiqueta)
	{
		etiqueta.append(`
			<div class='col-4 align-self-start my-1' style='float:left;'>
				<div class="card" style="width: 18rem;">
					<img src=" ${respuesta.Poster}" class="card-img-top" alt="...">
					<div class="card-body">
						<input type='hidden' id='name' value='${respuesta.Title}' name='name'>
						<h5 class="card-title" >Titulo:  ${respuesta.Title}</h5>
					</div>
					<ul class="list-group list-group-flush">
						<li class="list-group-item">Anio de lanzamiento:${respuesta.Year}</li>
					</ul>
				</div>
			</div>`);
	}

	//hacer las peticiones ajax por cada nombre de las peliculas del usuario
	for(let i in peliculas)
	{
		peticionAjaxGet(peliculas[i],url,key,contenido);
	}

	//funciones para buscar nueva pelcula
	var botonMas = $('#botonMas');
	var formMas = $('#form');
	var cerrarFor = $('#botonForm');
	var inputNombre= $('.inputName');
	var detalleBusqueda=$('#busqueda');
	var divBusquedad =$('#divBusqueda');
	var cerrarBusqueda = $('#botonCerrar');
	//eventos
	inputNombre.keyup(()=>
	{
		    $('#error_name').css('display','none');
            inputNombre.css('border','1px solid #ccc');
	});

	botonMas.click(()=>
	{
		formMas.css('display','block');
		botonMas.css('display','none');
	});

	cerrarFor.click(()=>
	{
		formMas.css('display','none');
		botonMas.css('display','block');
	});

	cerrarBusqueda.click(()=>
	{
		botonMas.css('display','block');
		divBusquedad.css('display','none');
	});

	//evento submit para buscar la pelicula
	formMas.unbind().submit(function(evento)
    {
		detalleBusqueda.text("");
		evento.preventDefault();
		var nombre = inputNombre.val();
        if(!validarInput(inputNombre))
        {
            evento.preventDefault();
		}else
		{	
			peticionAjaxGet(nombre,url,key,detalleBusqueda);
			divBusquedad.css('display','block');
			formMas.css('display','none');	
		}
	});
	//validar el formulario
	//funcion para valdiar el form
    function validarInput(input)
    {
        var valor = input.val();
        if(valor.trim()==null || valor.trim().length==0)
        {
			var errorName = $('#error_name');
            input.css('border','1px solid red');
			errorName.text('*Ingresar el nombre')
					  .css('color','red');
			
            return false;    
        }else
        {
            return true;
        }
	}

	var formGuardar=$('#divBusqueda');

	formGuardar.unbind().submit((event)=>
	{
		var nombre =$('#divBusqueda #name').val();
		var repetida;
		

		for(let i in peliculas)
		{
			
			peliculas[i]=peliculas[i].toUpperCase();
			
			if(peliculas[i]==nombre.toUpperCase())
			{	
				
				event.preventDefault();
				repetida=true;
			}
		}
		if(!repetida)
		{
			var respuesta = confirm('seguro quiere agregar esta pelicula');
			if(respuesta)
			{
				alert('pelicula agregada');
			}
		}else
		{
			event.preventDefault();
			alert("Esta pelicula ya existe");
		}
	});

});

var nombre = document.querySelector('#name');
console.log(nombre);

